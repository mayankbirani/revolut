//
//  RateService.swift
//  Revolut
//
//  Created by Mayank Birani on 31/10/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import Foundation

struct RateService: RateServiceProtocol {
    var pairs: [String]
    
    init(pairs: [String]) {
        self.pairs = pairs
    }
    
    func getExchangeRates(successHandler: @escaping SuccessRateHandler, errorHandler: @escaping ErrorHandler) {
        let rate = RequestRouter.rate(pairs: self.pairs)
        DataService.getData(request: rate, completionHandler: { (data) in
            if let json = try! JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                let rate = Rate(data: json)
                successHandler(rate)
            } else {
                errorHandler(Global.genericError)
            }
        }) { (error) in
            errorHandler(error)
        }
    }
}
