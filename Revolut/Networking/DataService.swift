//
//  DataService.swift
//  Dazn
//
//  Created by Mayank Birani on 21/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import Foundation

class DataService {
    
    static func getData(request: RequestRouter, completionHandler: @escaping ServiceCompletionHandler, errorHandler: @escaping ErrorHandler) {
        
        let requestValues = request.values()
        let url = requestValues.url
        var request = URLRequest(url: url)
        request.httpBody = requestValues.httpBody
        request.httpMethod = requestValues.httpMethod.rawValue
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let error = error {
                errorHandler(error.localizedDescription)
                print("error: \(error)")
            } else {
                if let response = response as? HTTPURLResponse {
                    print("statusCode: \(response.statusCode)")
                }
                if let data = data {
                    completionHandler(data)
                } else {
                    errorHandler("error.localizedDescription")
                }
            }
        }
        task.resume()
    }
}

typealias ServiceCompletionHandler = (_ result: Data) -> Void
typealias SuccessCurrencyHandler = (_ result: Currency) -> Void
typealias SuccessRateHandler = (_ result: Rate) -> Void
typealias ErrorHandler = (_ error: String) -> Void
