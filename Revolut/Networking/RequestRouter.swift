//
//  RequestRouter.swift
//  Dazn
//
//  Created by Mayank Birani on 25/06/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
}

let rateUrl = "https://europe-west1-revolut-230009.cloudfunctions.net/revolut-ios"
let currencyUrl = "https://demo6665281.mockable.io/currencies"

enum RequestRouter {
    case currency
    case rate(pairs: [String])
    
    func values()->(url: URL, httpMethod: HTTPMethod, httpBody:Data?) {
        
        switch self {
        case .currency :
            let url = URL(string: currencyUrl)!
            return (url: url, httpMethod: .get, httpBody: nil)
            
        case .rate(let pairs):
            var paramString = ""
            for (index, d) in pairs.enumerated() {
                let param = ["pairs": d]
                paramString += param.percentEscaped()
                if index != pairs.count-1 {
                    paramString += "&"
                }
            }
            let httpBody = paramString.data(using: .utf8)
            let url = URL(string: rateUrl)!
            return (url: url, httpMethod: .post, httpBody: httpBody)
        }
    }
}
