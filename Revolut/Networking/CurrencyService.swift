//
//  CurrencyService.swift
//  Revolut
//
//  Created by Mayank Birani on 31/10/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import Foundation

struct CurrencyService: CurrencyServiceProtocol {

    func getCurrencies(successHandler: @escaping SuccessCurrencyHandler, errorHandler: @escaping ErrorHandler) {
        let currencyRouter = RequestRouter.currency
        DataService.getData(request: currencyRouter, completionHandler: { (data) in
            if let currency = decodeWithJson(type: Currency.self, data: data) {
                successHandler(currency)
            } else {
                errorHandler(Global.genericError)
            }
        }) { (error) in
            errorHandler(error)
        }
    }
}
