//
//  JsonDecoder.swift
//  Revolut
//
//  Created by Mayank Birani on 31/10/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import Foundation

func decodeWithJson<T: Codable>(type: T.Type, data: Data?) -> (T?) {
    guard let data = data else {
        return nil
    }
    let decode = JSONDecoder()
    if let typeData = try? decode.decode(type, from: data) {
        return typeData
    }
    return nil
}
