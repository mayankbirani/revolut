//
//  Util.swift
//  Revolut
//
//  Created by Mayank Birani on 07/11/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import UIKit

typealias ActionCompletionHandler = (_ isOk: Bool) -> Void

enum AlertActionType: String {
    case none
    case ok
    case cancel
    case done
    case proceed
    case yes
    case no
}

func showAlertController(view: UIViewController, message: String, actionType: [AlertActionType], completion: @escaping ActionCompletionHandler) {
    
    let controller = UIAlertController(title: "Revolut", message: message, preferredStyle: .alert)
    for action in actionType {
        let actionStr = action.rawValue
        let action = UIAlertAction(title: actionStr, style: .default) { (UIAlertAction) in
            let first = actionType.first ?? .none
            completion(first == action)
        }
        controller.addAction(action)
    }
    view.present(controller, animated: true, completion: nil)
}


