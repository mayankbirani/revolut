//
//  CurrencyViewModel.swift
//  Revolut
//
//  Created by Mayank Birani on 30/10/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import Foundation

class CurrencyViewModel: CurrencyViewModelProtocol {
    
    var delegate: CurrencyCallBack
    var currency: Currency?
    var service: CurrencyService?
    
    required init(delegate: CurrencyCallBack) {
        self.delegate = delegate
        service = CurrencyService()
    }
    
    func getCurrencies() {
        service?.getCurrencies(successHandler: { [weak self] (currency) in
            self?.currency = currency
            self?.delegate.didCurrencySuccess(currency: currency)
        }, errorHandler: { [weak self] (error) in
            self?.delegate.didCurrencyFailure(error: error)
        })
    }
}
