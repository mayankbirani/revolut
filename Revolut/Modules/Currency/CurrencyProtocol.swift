//
//  CurrencyProtocol.swift
//  Revolut
//
//  Created by Mayank Birani on 30/10/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

protocol CurrencyCallBack: class {
    func didCurrencySuccess(currency: Currency)
    func didCurrencyFailure(error: String)
}

protocol CurrencyViewProtocol: class {
    var viewModel: CurrencyViewModelProtocol? { get set }
}

protocol CurrencyViewModelProtocol: class {
    init(delegate: CurrencyCallBack)
    var delegate: CurrencyCallBack { get set }
    var currency: Currency? { get set }
    var service: CurrencyService? { get set }
    func getCurrencies()
}

protocol CurrencyServiceProtocol {
    func getCurrencies(successHandler: @escaping SuccessCurrencyHandler, errorHandler: @escaping ErrorHandler)
}

