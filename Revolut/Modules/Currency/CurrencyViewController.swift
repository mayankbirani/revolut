//
//  CurrencyViewController.swift
//  Revolut
//
//  Created by Mayank Birani on 30/10/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import UIKit

class CurrencyViewController: UIViewController, CurrencyViewProtocol  {
    
    @IBOutlet weak var currencyTableView: UITableView!
    
    var viewModel: CurrencyViewModelProtocol?
    var selectedPairs: [String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Currencies"
        viewModel = CurrencyViewModel(delegate: self)
        viewModel?.getCurrencies()
    }
}

extension CurrencyViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.currency?.currencies.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CurrencyTableViewCell") as? CurrencyTableViewCell else {
            return UITableViewCell()
        }
        
        let currency = viewModel?.currency?.currencies[indexPath.row] ?? ""
        if selectedPairs.contains(currency) {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        cell.configureCell(currencyTitle: currency)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedPairs.append(viewModel?.currency?.currencies[indexPath.row] ?? "")
        if selectedPairs.count == 2 {
            let onePair = selectedPairs.joined(separator: "")
            Global.getRatePairs.append(onePair)
            self.navigationController?.popViewController(animated: true)
        } else {
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
}

extension CurrencyViewController: CurrencyCallBack {
    func didCurrencySuccess(currency: Currency) {
        DispatchQueue.main.async {
            self.currencyTableView.reloadData()
        }
    }
    
    func didCurrencyFailure(error: String) {
        showAlertController(view: self, message: error, actionType: [.ok]) { _ in }
    }
}
