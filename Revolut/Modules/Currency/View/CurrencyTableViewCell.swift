//
//  CurrencyTableViewCell.swift
//  Revolut
//
//  Created by Mayank Birani on 07/11/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import UIKit

class CurrencyTableViewCell: UITableViewCell {

    @IBOutlet weak var currencyTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(currencyTitle: String) {
        self.currencyTitle.text = currencyTitle
    }

}
