//
//  RateViewModel.swift
//  Revolut
//
//  Created by Mayank Birani on 30/10/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import Foundation

class RateViewModel: RateViewModelProtocol {
    
    var delegate: RateCallBack
    var service: RateService?
    var rate: Rate?
    
    required init(delegate: RateCallBack) {
        self.delegate = delegate
    }
    
    func getExchangeRates(ratePairs: [String]) {
        
        guard ratePairs.count > 0 else {
            self.delegate.noCurrencySelected()
            return
        }
        
        service = RateService(pairs: ratePairs)
        service?.getExchangeRates(successHandler: { [weak self] (rate) in
            self?.rate = rate
            self?.delegate.didRateSuccess(rates: rate.rate)
        }, errorHandler: { [weak self] (error) in
            self?.delegate.didRateFailure(error: error)
        })
    }
}
