//
//  RateViewController.swift
//  Revolut
//
//  Created by Mayank Birani on 30/10/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import UIKit

class RateViewController: UIViewController, RateViewProtocol {
    
    @IBOutlet weak var rateTableView: UITableView!
    @IBOutlet weak var addCurrencyView: UIView!

    var viewModel: RateViewModelProtocol?
    var timer = Timer()
    
    override func viewDidLoad() {
        viewModel = RateViewModel(delegate: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel?.getExchangeRates(ratePairs: Global.getRatePairs)
        scheduleTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer.invalidate()
    }
    
    @IBAction func addCurrencyImageTapped(_ sender: Any) {
        showCurrencyController()
    }
    
    @IBAction func addCurrencyHeaderTapped(_ sender: Any) {
        showCurrencyController()
    }
    
    @IBAction func addCurrencyTapped(sender: UIButton) {
        showCurrencyController()
    }
    
    fileprivate func showCurrencyController() {
        self.performSegue(withIdentifier: "ShowCurrencyView", sender: nil)
    }
    
    fileprivate func deleteRates(index: Int) {
        if let index = Global.getRatePairs.firstIndex(of: self.viewModel?.rate?.rate[index].rateId ?? "") {
            Global.getRatePairs.remove(at: index)
        }
        self.viewModel?.rate?.rate.remove(at: index)
        rateTableView.reloadData()
    }
    
    fileprivate func scheduleTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: 3, repeats: true, block: { [weak self] (timer) in
            self?.viewModel?.getExchangeRates(ratePairs: Global.getRatePairs)
        })
    }
}

extension RateViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.rate?.rate.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RateTableViewCell") as? RateTableViewCell else {
            return UITableViewCell()
        }

        if let rate = viewModel?.rate?.rate[indexPath.row] {
            cell.configureCell(rate: rate)
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RateHeaderView") else {
            return UITableViewCell()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView,
                            editActionsForRowAt indexPath: IndexPath)
        -> [UITableViewRowAction]? {
            
            let deleteTitle = NSLocalizedString("Delete", comment: "Delete action")
            let deleteAction = UITableViewRowAction(style: .destructive,
                                                    title: deleteTitle) { (_, indexPath) in
                                                        self.deleteRates(index: indexPath.row)
                                                        
            }

            return [deleteAction]
    }
}

extension RateViewController: RateCallBack {
    func didRateSuccess(rates: [Rates]) {
        DispatchQueue.main.async {
            self.addCurrencyView.isHidden = true
            self.rateTableView.isHidden = false
            self.rateTableView.reloadData()
        }
    }
    
    func didRateFailure(error: String) {
        showAlertController(view: self, message: error, actionType: [.ok]) { _ in }
    }
    
    func noCurrencySelected() {
        DispatchQueue.main.async {
            self.addCurrencyView.isHidden = false
            self.rateTableView.isHidden = true
        }
    }
}
