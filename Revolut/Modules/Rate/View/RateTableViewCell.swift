//
//  RateTableViewCell.swift
//  Revolut
//
//  Created by Mayank Birani on 07/11/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import UIKit

class RateTableViewCell: UITableViewCell {

    @IBOutlet weak var rateFromLabel: UILabel!
    @IBOutlet weak var rateDifferenceLabel: UILabel!
    @IBOutlet weak var rateToLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(rate: Rates) {
        let fromRate = getSubString(str: rate.rateId, startOffset: 0, endOffset: 3)
        let toRate = getSubString(str: rate.rateId, startOffset: 3, endOffset: 6)
        
        self.rateFromLabel.text = "1 \(fromRate)"
        self.rateToLabel.text = toRate
        self.rateDifferenceLabel.text = String(rate.rateDifference)
    }
    
    fileprivate func getSubString(str: String, startOffset: Int, endOffset: Int) -> String {
        let start = str.index(str.startIndex, offsetBy: startOffset)
        let end = str.index(str.startIndex, offsetBy: endOffset)
        let range = start..<end
        return String(str[range])
    }
}
