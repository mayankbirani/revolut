//
//  RateProtocol.swift
//  Revolut
//
//  Created by Mayank Birani on 30/10/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import Foundation

protocol RateCallBack: class {
    func didRateSuccess(rates: [Rates])
    func didRateFailure(error: String)
    func noCurrencySelected()
}

protocol RateViewProtocol: class {
    var viewModel: RateViewModelProtocol? { get set }
}

protocol RateViewModelProtocol: class {
    init(delegate: RateCallBack)
    var delegate: RateCallBack { get set }
    var rate: Rate? { get set }
    var service: RateService? { get set }
    func getExchangeRates(ratePairs: [String])
}

protocol RateServiceProtocol {
    var pairs: [String] { get set }
    func getExchangeRates(successHandler: @escaping SuccessRateHandler, errorHandler: @escaping ErrorHandler)
}
