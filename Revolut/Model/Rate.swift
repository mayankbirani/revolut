//
//  Rate.swift
//  Revolut
//
//  Created by Mayank Birani on 30/10/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import Foundation

struct Rate {
    var rate: [Rates]
    
    init(data: [String: Any]) {
        rate = []
        for dict in data {
            let sRate = Rates(rateId: dict.key, rateDifference: (dict.value as? Double) ?? 0.0)
            rate.append(sRate)
        }
    }
}

struct Rates {
    var rateId: String
    var rateDifference: Double
}

