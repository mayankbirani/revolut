//
//  Currency.swift
//  Revolut
//
//  Created by Mayank Birani on 30/10/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import Foundation

struct Currency: Codable {
    var currencies: [String]
}
