//
//  Globals.swift
//  Revolut
//
//  Created by Mayank Birani on 07/11/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import Foundation

let UserDefault = UserDefaults.standard

enum UserDefaultKeys: String {
    case ratePairsKey
}

struct Global {
    static var getRatePairs: [String] {
        get {
            if let ratePairs = UserDefault.value(forKey: UserDefaultKeys.ratePairsKey.rawValue) as? [String] {
                return ratePairs
            }
            return []
        }
        set {
            UserDefault.set(newValue, forKey: UserDefaultKeys.ratePairsKey.rawValue)
            UserDefault.synchronize()
        }
    }
    
    static let genericError = "Something went wrong!"
}


