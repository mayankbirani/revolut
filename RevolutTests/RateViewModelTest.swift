//
//  RateViewModelTest.swift
//  RevolutTests
//
//  Created by Mayank Birani on 07/11/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import XCTest
@testable import Revolut

class RateViewModelTest: XCTestCase {

    let rateViewModel = RateViewModel(delegate: MockRateView())
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testNoGetExchangeRates() {
        rateViewModel.getExchangeRates(ratePairs: [])
    }
    
    func testGetExchangeRates() {
        rateViewModel.getExchangeRates(ratePairs: ["GBPUSD"])
    }
    
    func testWrongGetExchangeRates() {
        rateViewModel.getExchangeRates(ratePairs: ["GBPUS"])
    }

}

fileprivate class MockRateView: RateCallBack {
    func didRateSuccess(rates: [Rates]) {
    }
    func didRateFailure(error: String) {
    }
    func noCurrencySelected() {
    }
}
