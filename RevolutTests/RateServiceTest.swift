//
//  RateServiceTest.swift
//  RevolutTests
//
//  Created by Mayank Birani on 07/11/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import XCTest
@testable import Revolut

class RateServiceTest: XCTestCase  {

    override func setUp() {
    }

    override func tearDown() {
        super.tearDown()
    }
    
    func testGetExchangeRates() {
        let service : MockRateService = MockRateService(pairs: ["GBPUSD"])
        service.getExchangeRates(successHandler: { (rate) in
            XCTAssert(rate.rate.first?.rateId == "GBPUSD", "True")
        }) { (error) in
        }
    }
    
    func testErrorGetExchangeRates() {
        let service : MockRateService = MockRateService(pairs: [])
        service.getExchangeRates(successHandler: { (rate) in
        }) { (error) in
            XCTAssert(error == Global.genericError)
        }
    }

}

fileprivate class MockRateService : RateServiceProtocol {
    
    var pairs: [String]
    
    init(pairs: [String]) {
        self.pairs = pairs
    }
    func getExchangeRates(successHandler: @escaping SuccessRateHandler, errorHandler: @escaping ErrorHandler) {
        if pairs.count > 0 {
            let rate = Rate.init(data: ["GBPUSD":2])
            successHandler(rate)
        } else {
            errorHandler(Global.genericError)
        }
        
    }
}
