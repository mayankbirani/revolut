//
//  CurrencyServiceTest.swift
//  RevolutTests
//
//  Created by Mayank Birani on 07/11/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import XCTest
@testable import Revolut

class CurrencyServiceTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testGetCurrencies() {
        let service : MockCurrencyService = MockCurrencyService()
        service.isCorrectDetails = true
        service.getCurrencies(successHandler: { (currency) in
            XCTAssert(currency.currencies.first == "USD", "True")
        }) { (error) in
        }
    }
    
    func testErrorGetCurrencies() {
        let service : MockCurrencyService = MockCurrencyService()
        service.isCorrectDetails = false
        service.getCurrencies(successHandler: { (currency) in
        }) { (error) in
            XCTAssert(error == Global.genericError)
        }
    }
}

fileprivate class MockCurrencyService : CurrencyServiceProtocol {
    
    var isCorrectDetails: Bool = true
    
    func getCurrencies(successHandler: @escaping SuccessCurrencyHandler, errorHandler: @escaping ErrorHandler) {
        if isCorrectDetails {
            let currency = Currency(currencies: ["USD", "GBP"])
            successHandler(currency)
        } else {
            errorHandler(Global.genericError)
        }
        
    }
}
