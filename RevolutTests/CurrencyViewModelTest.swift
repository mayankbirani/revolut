//
//  CurrencyViewModelTest.swift
//  RevolutTests
//
//  Created by Mayank Birani on 07/11/2019.
//  Copyright © 2019 Mayank Birani. All rights reserved.
//

import XCTest
@testable import Revolut

class CurrencyViewModelTest: XCTestCase {

    let viewModel = CurrencyViewModel(delegate: MockCurrencyView())

    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testGetCurrencies() {
        viewModel.getCurrencies()
    }
}

fileprivate class MockCurrencyView: CurrencyCallBack {
    func didCurrencySuccess(currency: Currency) {
    }
    func didCurrencyFailure(error: String) {
    }
}
